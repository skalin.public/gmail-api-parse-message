"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeNonPrint = exports.copyGmail = exports.getEmptyEmail = void 0;
function getEmptyEmail() {
    const email = {
        id: '',
        size: -1,
        threadId: '',
        historyId: '',
        subject: '',
        textPlain: '',
        textHtml: '',
        internalDate: -1,
        sentDate: -1,
        from: { name: '', email: '', isValid: true },
        to: [],
        cc: [],
        bcc: [],
        labels: [],
        labelIds: [],
        snippet: '',
        attachments: [],
        headers: new Map(),
        isUnread: true
    };
    return email;
}
exports.getEmptyEmail = getEmptyEmail;
/** returns a fresh copy of IGmail */
function copyGmail(gmail) {
    const copy = JSON.parse(JSON.stringify(gmail));
    copy.headers = new Map(gmail.headers);
    return copy;
}
exports.copyGmail = copyGmail;
/** remove non printing chars, space, just intended for use for string comparison with multilines  */
function removeNonPrint(s) {
    return s.replace(/[^A-Za-zæøåÆØÅ0-9$§!"#€%&/\[\]\?{}()<>=@,.;':]/g, '');
}
exports.removeNonPrint = removeNonPrint;
;
//# sourceMappingURL=constants.js.map