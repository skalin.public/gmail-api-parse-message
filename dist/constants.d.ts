import { IEmail } from './iface/iemail';
export declare function getEmptyEmail(): IEmail;
/** returns a fresh copy of IGmail */
export declare function copyGmail(gmail: IEmail): IEmail;
/** remove non printing chars, space, just intended for use for string comparison with multilines  */
export declare function removeNonPrint(s: string): string;
