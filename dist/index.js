"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEmptyEmail = exports.ParseGmailApi = void 0;
var parse_gmail_api_1 = require("./parse-gmail-api");
Object.defineProperty(exports, "ParseGmailApi", { enumerable: true, get: function () { return parse_gmail_api_1.ParseGmailApi; } });
var constants_1 = require("./constants");
Object.defineProperty(exports, "getEmptyEmail", { enumerable: true, get: function () { return constants_1.getEmptyEmail; } });
//# sourceMappingURL=index.js.map