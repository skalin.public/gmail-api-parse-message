"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParseGmailApi = void 0;
const b64Decode = require('base-64').decode;
const constants_1 = require("./constants");
const klingsten_snippets_1 = require("klingsten-snippets");
/** parses gmail api response to a IGmail object - typescript */
class ParseGmailApi {
    constructor() { }
    /**  parses a gmail-api message.resource object, and returns a IEmail Object
     * @param   messages.resource
     * @returns IEmail
     *
     * messages.resource: https://developers.google.com/gmail/api/v1/reference/users/messages#resource
     */
    parseMessage(gmailApiResp) {
        // its not evident that we only need the result, so we test to see what we got
        const resp = gmailApiResp.result || gmailApiResp;
        const gmail = this.parseRawGmailResponse(resp);
        this.parseHeaders(gmail);
        return gmail;
    }
    /** Decodes a URLSAFE Base64 string to its original representation */
    urlB64Decode(s) {
        return s ? decodeURIComponent(escape(b64Decode(s.replace(/\-/g, '+').replace(/\_/g, '/')))) : '';
    }
    /** check whether an email address is valid, exposes func from klingsten snippets */
    isEmailValid(s) {
        return klingsten_snippets_1.Strings.isEmailValid(s);
    }
    /** Parses a Gmail API response to a iGmail object
     * 'to', 'from' 'cc', 'subject' are stored property 'headers'  */
    parseRawGmailResponse(gmailApiResp) {
        const email = constants_1.getEmptyEmail();
        email.id = gmailApiResp.id;
        email.threadId = gmailApiResp.threadId;
        email.labelIds = gmailApiResp.labelIds;
        email.snippet = gmailApiResp.snippet;
        email.historyId = gmailApiResp.historyId;
        email.isUnread = this.isEmailUnread(email.labelIds);
        if (gmailApiResp.internalDate) {
            email.internalDate = parseInt(gmailApiResp.internalDate, 10);
        }
        const payload = gmailApiResp.payload;
        if (!payload) {
            return email;
        }
        let headers = this.indexHeaders(payload.headers);
        email.headers = headers;
        let parts = [payload];
        let firstPartProcessed = false;
        while (parts.length !== 0) {
            const part = parts.shift();
            if (part.parts) {
                parts = parts.concat(part.parts);
            }
            if (firstPartProcessed) {
                headers = this.indexHeaders(part.headers);
            }
            if (!part.body) {
                continue;
            }
            const contentDisposition = headers.get('content-disposition');
            const isHtml = part.mimeType && part.mimeType.indexOf('text/html') !== -1;
            const isPlain = part.mimeType && part.mimeType.indexOf('text/plain') !== -1;
            const isAttachment = this.isAttachment(part);
            const isInline = contentDisposition && contentDisposition.indexOf('inline') !== -1;
            if (isHtml && !isAttachment) {
                email.textHtml = this.urlB64Decode(part.body.data);
            }
            else if (isPlain && !isAttachment) {
                email.textPlain = this.urlB64Decode(part.body.data);
            }
            else if (isAttachment) {
                const body = part.body;
                if (!email.attachments) {
                    email.attachments = [];
                }
                email.attachments.push({
                    filename: part.filename,
                    mimeType: part.mimeType,
                    size: body.size,
                    attachmentId: body.attachmentId,
                    headers: this.indexHeaders(part.headers)
                });
            }
            else if (isInline) {
                const body = part.body;
                if (!email.inline) {
                    email.inline = [];
                }
                email.inline.push({
                    filename: part.filename,
                    mimeType: part.mimeType,
                    size: body.size,
                    attachmentId: body.attachmentId,
                    headers: this.indexHeaders(part.headers)
                });
            }
            firstPartProcessed = true;
        }
        return email;
    }
    ;
    isAttachment(part) {
        if (part.body.attachmentId && part.filename) {
            return true;
        }
        return false;
    }
    /** Converts email headers (such  as "subject":"Email Subj" into
     *  map<sting,string> with the key in lowercase. Its further
     * combines headers such as "received_from" into a single key value pair  */
    indexHeaders(headers) {
        const result = new Map();
        if (!headers) { // missing headers could come from Gmail draft, or an outgoing message
            return result;
        }
        headers.forEach((v) => {
            let value = result.get(v.name.toLowerCase());
            if (!value) {
                value = '';
            }
            result.set(v.name.toLowerCase(), value + v.value);
        });
        return result;
    }
    /** for testing parseAddresses purpose only
     * does not mutate object Email, return a parsed copy
     */
    test_parseAddresses(email) {
        const copy = constants_1.copyGmail(email);
        return this.parseHeaders(copy);
    }
    /**  mutates/parse IEmail headers (such as 'to', 'subject')
     * to to, cc, from, subject attributes
    * @param { IEmail } with 'to' 'from' 'cc' 'subject' 'bcc' set in the headers attributes
    */
    parseHeaders(email) {
        email.from = this.parseReceivers(email.headers.get('from'), false)[0] || []; // should be ok?
        email.to = this.parseReceivers(email.headers.get('to'), false);
        email.cc = this.parseReceivers(email.headers.get('cc'), false);
        email.bcc = this.parseReceivers(email.headers.get('bcc'), false);
        email.sentDate = parseInt(email.headers.get('date') || '', 10);
        if (!email.sentDate) {
            email.sentDate = email.internalDate;
        }
        email.subject = email.headers.get('subject') || '';
        email.attachments = email.attachments || email.inline || [];
        // clean up
        email.inline = []; // merged with attachments into attachments
        const removeHeadersFromEmail = ['from', 'to', 'cc', 'bcc', 'subject'];
        for (let i = 0; i < removeHeadersFromEmail.length; i++) {
            email.headers.delete(removeHeadersFromEmail[i]);
        }
        return email;
    }
    /** converts are string container emails, and returns them as IReceivers[]
     *  Emails are by default checked for validity, however optional parameter
     *  checkIfEmailIsValid can be set to false, whereby emails are not checked,
     *  and isValid is set to true.  ParseGmailApi.parseMessage() always set isValid=true
     *  without checking  */
    parseReceivers(receiverStr = "", checkIfEmailIsValid = true) {
        const receivers = [];
        if (!receiverStr) {
            return receivers;
        }
        // split string of received into array
        const strArrReceivers = klingsten_snippets_1.Strings.splitExceptQuotes(receiverStr);
        if (!strArrReceivers) {
            return receivers;
        }
        // split name from email address
        for (let i = 0; i < strArrReceivers.length; i++) {
            const resp = klingsten_snippets_1.Strings.splitNameFromEmail(strArrReceivers[i]);
            resp.name = this.removeUnwantedCharsFromName(resp.name);
            receivers.push({
                name: resp.name,
                email: resp.email,
                isValid: checkIfEmailIsValid ? this.isEmailValid(resp.email) : true
            });
        }
        return receivers;
    }
    removeUnwantedCharsFromName(s) {
        const re = /(['"<>()!*;,#?]+)/gm;
        return s.replace(re, '');
    }
    /** check if labels "unread" has been set */
    isEmailUnread(labels) {
        if (!labels) {
            return false;
        }
        const UNREAD = 'unread';
        for (let i = 0; i < labels.length; i++) {
            if (labels[i] === UNREAD) {
                return true;
            }
        }
        return false;
    }
}
exports.ParseGmailApi = ParseGmailApi;
//# sourceMappingURL=parse-gmail-api.js.map