import { IEmail } from './iface/iemail';
import { IReceiver } from './iface/ireceiver';
/** parses gmail api response to a IGmail object - typescript */
export declare class ParseGmailApi {
    constructor();
    /**  parses a gmail-api message.resource object, and returns a IEmail Object
     * @param   messages.resource
     * @returns IEmail
     *
     * messages.resource: https://developers.google.com/gmail/api/v1/reference/users/messages#resource
     */
    parseMessage(gmailApiResp: any): IEmail;
    /** Decodes a URLSAFE Base64 string to its original representation */
    urlB64Decode(s: string): string;
    /** check whether an email address is valid, exposes func from klingsten snippets */
    isEmailValid(s: string): boolean;
    /** Parses a Gmail API response to a iGmail object
     * 'to', 'from' 'cc', 'subject' are stored property 'headers'  */
    private parseRawGmailResponse;
    private isAttachment;
    /** Converts email headers (such  as "subject":"Email Subj" into
     *  map<sting,string> with the key in lowercase. Its further
     * combines headers such as "received_from" into a single key value pair  */
    private indexHeaders;
    /** for testing parseAddresses purpose only
     * does not mutate object Email, return a parsed copy
     */
    test_parseAddresses(email: IEmail): IEmail;
    /**  mutates/parse IEmail headers (such as 'to', 'subject')
     * to to, cc, from, subject attributes
    * @param { IEmail } with 'to' 'from' 'cc' 'subject' 'bcc' set in the headers attributes
    */
    private parseHeaders;
    /** converts are string container emails, and returns them as IReceivers[]
     *  Emails are by default checked for validity, however optional parameter
     *  checkIfEmailIsValid can be set to false, whereby emails are not checked,
     *  and isValid is set to true.  ParseGmailApi.parseMessage() always set isValid=true
     *  without checking  */
    parseReceivers(receiverStr?: string, checkIfEmailIsValid?: boolean): IReceiver[];
    private removeUnwantedCharsFromName;
    /** check if labels "unread" has been set */
    isEmailUnread(labels: string[]): boolean;
}
